package com.example.marketing;

import org.json.JSONException;
import org.json.JSONObject;

public class Coordenada {

    public String latitud;
    public String longitud;
    public String Nombre;
    public String tokenDispositivo;

    public Coordenada(JSONObject objetoJSON) throws JSONException {
        this.latitud = objetoJSON.getString("latitud");
        this.longitud = objetoJSON.getString("longitud");
        this.tokenDispositivo = objetoJSON.getString("tokenDispositivo");
        Nombre = objetoJSON.getString("Nombre");
    }

    public Coordenada(String latitud, String longitud, String nombre) {
        this.latitud = latitud;
        this.longitud = longitud;
        Nombre = nombre;
    }

    public String getTokenDispositivo() {
        return tokenDispositivo;
    }

    public void setTokenDispositivo(String tokenDispositivo) {
        this.tokenDispositivo = tokenDispositivo;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}