package com.example.marketing;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Notificacion extends AppCompatActivity {
    Button especifico, general;
    ImageButton imageButton;
    EditText titulo, descripcion;
    String token ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificacion);
        especifico = (Button) findViewById(R.id.especifico);
        general = (Button) findViewById(R.id.general);
        titulo = (EditText) findViewById(R.id.titulo);
        descripcion = (EditText) findViewById(R.id.descripcion);
        imageButton = (ImageButton) findViewById(R.id.imageButton);

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.i("tag", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        token = task.getResult().getToken();
                    }
                });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferencias.edit();
                String Nombre = preferencias.getString("Nombre", "");
                String TokenCercano = preferencias.getString("TokenCercano", "");
                if(Nombre != "" && TokenCercano != ""){
                    editor.remove("TokenCercano");
                    editor.remove("Nombre");
                    editor.commit();
                    Toast.makeText(Notificacion.this, "Cerraste sesion", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Notificacion.this, PantallaLogin.class));
                }
            }
        });

        especifico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llamarEspecifico();
            }
        });
        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llamarGeneral();
            }
        });
    }


    private void llamarEspecifico() {
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try{
            SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
            String token = preferencias.getString("TokenCercano", "");
            if (token != "" && token != null){
                json.put("to",token);
                JSONObject notificacion = new JSONObject();
                notificacion.put("titulo", titulo.getText());
                notificacion.put("detalle", descripcion.getText());
                json.put("data", notificacion);
                String URL = "https://fcm.googleapis.com/fcm/send";
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, json,null,null){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> header = new HashMap<>();
                        header.put("content-type", "application/json");
                        header.put("authorization", "key=AAAAdW2JuT4:APA91bE0kkHJRVBjgeIXMcLFaJ6DcDcWyeLd3MKUrahg4Ok_WgQGolmj_iTF362OOHdcveHZ8L0jxcOpWDgA_LUXUMaELonq9227zNzPWhqHBdCH56xH6O1rHC6DaKuh3KIeF0Pzrhr0");
                        return header;
                    }
                };
                myrequest.add(request);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void llamarGeneral() {
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try{
            json.put("to","/topics/"+"atodos");
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo", titulo.getText());
            notificacion.put("detalle", descripcion.getText());
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, json,null,null){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAAdW2JuT4:APA91bE0kkHJRVBjgeIXMcLFaJ6DcDcWyeLd3MKUrahg4Ok_WgQGolmj_iTF362OOHdcveHZ8L0jxcOpWDgA_LUXUMaELonq9227zNzPWhqHBdCH56xH6O1rHC6DaKuh3KIeF0Pzrhr0");
                    return header;
                }
            };
            myrequest.add(request);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
