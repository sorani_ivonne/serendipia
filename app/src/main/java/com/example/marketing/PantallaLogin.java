package com.example.marketing;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PantallaLogin extends AppCompatActivity {
    Button login;
    EditText correo, contrasena;
    TextView textView5;
    public String token = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_login);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.i("tag", "Falló el ID", task.getException());
                            return;
                        }
                        token = task.getResult().getToken();
                    }
                });
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String nombre = preferencias.getString("Nombre", "");
        if(nombre != ""){
            Toast.makeText(PantallaLogin.this, "Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(PantallaLogin.this, PantallaMarcadores.class));
        }
        correo = (EditText) findViewById(R.id.correo);
        contrasena = (EditText) findViewById(R.id.contrasena);
        login = (Button) findViewById(R.id.login);
        textView5 = (TextView) findViewById(R.id.textView5);

        textView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PantallaLogin.this, PantallaRegistro.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarusuario("https://typical-ticks.000webhostapp.com/PersonaLogin.php?usuario="+correo.getText().toString()+"&password="+contrasena.getText().toString());
            }
        });
    }

    private void validarusuario(String URL) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (!response.isEmpty()) {
                    try{
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        if (msg.equals("true")){
                            guardarPreferencias(jsonObject.getString("nombre"));
                            Toast.makeText(PantallaLogin.this, "Entraste "+jsonObject.getString("nombre"), Toast.LENGTH_LONG).show();
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://typical-ticks.000webhostapp.com/ActualizarUbicacion.php?tokenDispositivo="+token+"&nombre="+jsonObject.getString("nombre")+"", new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if (!response.isEmpty()) {
                                        try{
                                            JSONObject jsonObject = new JSONObject(response);
                                            String msg = jsonObject.getString("estatus");
                                            if (msg.equals("true")){
                                                Toast.makeText(PantallaLogin.this, "Ubicación compartida", Toast.LENGTH_LONG).show();
                                            } else{
                                                Toast.makeText(PantallaLogin.this, "Fallo en la ubicacion", Toast.LENGTH_LONG).show();
                                            }
                                        } catch (Exception e){
                                            Toast.makeText(PantallaLogin.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    } else{
                                        Toast.makeText(PantallaLogin.this, "Peticion invalida", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(PantallaLogin.this, error.toString(), Toast.LENGTH_LONG).show();
                                }
                            }){
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    return super.getParams();
                                }
                            };
                            RequestQueue requestQueue = Volley.newRequestQueue(PantallaLogin.this);
                            requestQueue.add(stringRequest);
                            startActivity(new Intent(getApplicationContext(), PantallaMarcadores.class));
                        } else{
                            Toast.makeText(PantallaLogin.this, "Datos incorrectos", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e){
                        Toast.makeText(PantallaLogin.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else{
                    Toast.makeText(PantallaLogin.this, "Peticion invalida", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PantallaLogin.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parametros = new HashMap<String, String>();
                parametros.put("usuario", correo.getText().toString());
                parametros.put("password", contrasena.getText().toString());
                return parametros;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    public void guardarPreferencias(String Nombre){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String nombre = Nombre;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("Nombre", nombre);
        editor.commit();
    }
}
