package com.example.marketing;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PantallaRegistro extends AppCompatActivity {
    EditText correo, password, nombre;
    Button registrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registro);
        correo = (EditText) findViewById(R.id.correo);
        password = (EditText) findViewById(R.id.password);
        nombre = (EditText) findViewById(R.id.nombre);
        registrar = (Button) findViewById(R.id.registrar);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!correo.getText().equals("") && !password.getText().equals("") && !nombre.getText().equals("")){
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://typical-ticks.000webhostapp.com/CrearPersona.php?usuario="+correo.getText()+"&password="+password.getText()+"&latitud=1&longitud=1&Nombre="+nombre.getText()+"&tokenDispositivo=a", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (!response.isEmpty()) {
                                try{
                                    JSONObject jsonObject = new JSONObject(response);
                                    String msg = jsonObject.getString("mensaje");
                                    if (msg.equals("Persona creada")){
                                        Toast.makeText(PantallaRegistro.this, "Registro Exitoso, ahora puedes usar Serendipia", Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(getApplicationContext(), PantallaLogin.class));
                                    } else{
                                        Toast.makeText(PantallaRegistro.this, "Registro Exitoso, ahora puedes usar Serendipia", Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(getApplicationContext(), PantallaLogin.class));
                                    }
                                } catch (Exception e){
                                    Toast.makeText(PantallaRegistro.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } else{
                                Toast.makeText(PantallaRegistro.this, "Peticion invalida", Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(PantallaRegistro.this, error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> parametros = new HashMap<String, String>();
                            //parametros.put("usuario", correo.getText().toString());
                            //parametros.put("password", contrasena.getText().toString());
                            return parametros;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(PantallaRegistro.this);
                    requestQueue.add(stringRequest);
                }
            }
        });
    }
}
